$(function(){
	"use strict";
    $(".nav_btn").click(function(){
    	$(this).toggleClass("open");
    	$("nav ul").slideToggle(function(){
      		if(!$(".nav_btn").hasClass("open")){
        		$("nav ul").attr("style","");
        		$("body").css({
          			"background":"#f0f0f0",
        		});
				
        		$("main").css({
          			"opacity":"1"
        		});
      		}else{
        		$("body").css({
          			"background":"rgba(0,0,0,.75)",
        		});
        		$("main").css({
          			"opacity":".5"
        		});
      		}
    	});
  	});

	 $(".pagetop").click(function(){
	 	$("html,body").animate({scrollTop:0},500);
		return false;
	});

});
